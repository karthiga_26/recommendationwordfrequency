"""
Problem Statement: Word count using the text in the file
Need to find the word frequency for recommendations (Recommendation) per product (Product_Name). There will be more than one line per product.
The attached file has header row. dataset_review.csv
"""




#read input file
def readFile(fileLocation):
    file = open(fileLocation)
    data = file.read().split("\n")
    return data

#extract data for products and reviews
def extractData(data):
    del(data[0])
    productReviewData=[]
    for value in data[:-1]:
        lines = value.split(",")
        productReviewData.append((lines[6],lines[5]))
    return productReviewData

#append all the reviews for each product
def appendProductReviews(val1,val2):
    return val1 +" " +val2

def getAllReviewsEachProduct(products):
        productsReview = dict()
        for productReview in products:
            if productReview[0] in productsReview:
                val =appendProductReviews(productsReview[productReview[0]],productReview[1])
                productsReview[productReview[0]] = val
            else:
                productsReview[productReview[0]] = productReview[1]
        return productsReview

#calculate word frequency of reviews for that product and sort it based on the count
def wordFrequencyEachProduct(productReview):
    from collections import Counter
    for key in productReview:
        reviews = productReview[key].split()
        productReviewCount = Counter(reviews)
        productReview[key]=dict(sorted(productReviewCount.items(),key = lambda x:x[1],reverse=True))
    print(productReview)
    return productReview

#sort product name alphabetically
def sortProduct(product):
    alphabeticallySortProduct = dict(sorted(product.items(), key=lambda x: x[0].lower()))
    return alphabeticallySortProduct

# This method is used to solve problem statement
def sortedReviewCountforEachProduct(inputs):
    productReviewValues = extractData(inputs)
    entireProductsReviews = getAllReviewsEachProduct(productReviewValues)
    reviewsCount = wordFrequencyEachProduct(entireProductsReviews)
    output = sortProduct(reviewsCount)
    return output

#Driver Program
if __name__== "__main__":
    fileInputs = readFile('C:/Users/kdhanasekaran/PycharmProjects/ProductRecommendationWordFrequency/dataset_review.csv')
    print(sortedReviewCountforEachProduct(fileInputs))
