import unittest


class MyTestCase(unittest.TestCase):
    def test_getAllReviewsEachProduct(self):
        from RecommendWordFrequency import getAllReviewsEachProduct
        inputProductsReviews= [('Jumpsuit', 'I need one side pocket for putting my stuff'), ('Tshirt', 'these many pocket are not required '), ('Kurti', 'No recommendations its perfect'), ('Kurti', 'Need to have more colour options to make it perfect')]
        outputEntireProductReview = {'Jumpsuit': 'I need one side pocket for putting my stuff', 'Tshirt': 'these many pocket are not required ', 'Kurti': 'No recommendations its perfect Need to have more colour options to make it perfect'}
        self.assertEqual(getAllReviewsEachProduct(inputProductsReviews),outputEntireProductReview)

    def test_wordFrequencyEachProduct(self):
        from RecommendWordFrequency import wordFrequencyEachProduct
        inputProductReviewsWord = {'Jumpsuit': 'I need one side pocket for putting my stuff', 'Tshirt': 'these many pocket are not required ', 'Kurti': 'No recommendations its perfect Need to have more colour options to make it perfect'}
        outputProductReviewsCount ={'Jumpsuit': {'I': 1, 'need': 1, 'one': 1, 'side': 1, 'pocket': 1, 'for': 1, 'putting': 1, 'my': 1, 'stuff': 1}, 'Tshirt': {'these': 1, 'many': 1, 'pocket': 1, 'are': 1, 'not': 1, 'required': 1}, 'Kurti': {'perfect': 2, 'to': 2, 'No': 1, 'recommendations': 1, 'its': 1, 'Need': 1, 'have': 1, 'more': 1, 'colour': 1, 'options': 1, 'make': 1, 'it': 1}}
        self.assertEqual(wordFrequencyEachProduct(inputProductReviewsWord),outputProductReviewsCount)

    def test_sortProduct(self):
        from RecommendWordFrequency import sortProduct
        inputProducts = {'Jumpsuit': {'I': 1, 'need': 1, 'one': 1, 'side': 1, 'pocket': 1, 'for': 1, 'putting': 1, 'my': 1, 'stuff': 1}, 'Tshirt': {'these': 1, 'many': 1, 'pocket': 1, 'are': 1, 'not': 1, 'required': 1}, 'Kurti': {'perfect': 2, 'to': 2, 'No': 1, 'recommendations': 1, 'its': 1, 'Need': 1, 'have': 1, 'more': 1, 'colour': 1, 'options': 1, 'make': 1, 'it': 1}}
        outputSortedProducts = {'Jumpsuit': {'I': 1, 'need': 1, 'one': 1, 'side': 1, 'pocket': 1, 'for': 1, 'putting': 1, 'my': 1, 'stuff': 1}, 'Kurti': {'No': 1, 'recommendations': 1, 'its': 1, 'perfect': 2, 'Need': 1, 'to': 2, 'have': 1, 'more': 1, 'colour': 1, 'options': 1, 'make': 1, 'it': 1}, 'Tshirt': {'these': 1, 'many': 1, 'pocket': 1, 'are': 1, 'not': 1, 'required': 1}}
        self.assertEqual(sortProduct(inputProducts),outputSortedProducts)
if __name__ == '__main__':
    unittest.main()
